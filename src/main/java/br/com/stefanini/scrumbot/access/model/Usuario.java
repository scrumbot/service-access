package br.com.stefanini.scrumbot.access.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "usuario")
public class Usuario implements Serializable {

	private static final long serialVersionUID = 3595747764050708394L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@Column(name = "nome")
	private String nome;

	@Column(name = "usuario_telegran")
	private String usuarioTelegran;

	@ManyToMany(cascade = { CascadeType.PERSIST })
	@JoinTable(name = "perfil_usuario", 
			joinColumns = { @JoinColumn(name = "id_usuario") }, 
			inverseJoinColumns = {@JoinColumn(name = "id_perfil") })
	private List<Perfil> perfis = new ArrayList<Perfil>();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getUsuarioTelegran() {
		return usuarioTelegran;
	}

	public void setUsuarioTelegran(String usuarioTelegran) {
		this.usuarioTelegran = usuarioTelegran;
	}

	public List<Perfil> getPerfis() {
		return perfis;
	}

	public void setPerfis(List<Perfil> perfis) {
		this.perfis = perfis;
	}
	
	
}
