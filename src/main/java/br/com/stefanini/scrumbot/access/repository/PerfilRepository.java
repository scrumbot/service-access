package br.com.stefanini.scrumbot.access.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.stefanini.scrumbot.access.model.Perfil;

@Repository
public interface PerfilRepository extends CrudRepository<Perfil, Integer> {
	
	@Query("select perfil from Perfil perfil "
			+ " join perfil.projeto projeto "
			+ " join perfil.relatorios relatorios "
			+ " join perfil.usuarios usuarios"
			+ " where projeto.id = :idProjeto"
			+ " and  relatorios.id = :idRelatorio "
			+ " and usuarios.usuarioTelegran = :usuario")
	public Perfil obterPerfil(@Param("idProjeto") Integer idProjeto, 
							  @Param("idRelatorio") Integer idRelatorio, 
							  @Param("usuario") String usuario);

}
