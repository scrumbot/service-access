package br.com.stefanini.scrumbot.access.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.stefanini.scrumbot.access.dto.ProjetoDTO;
import br.com.stefanini.scrumbot.access.exception.BusinessException;
import br.com.stefanini.scrumbot.access.repository.ProjetoRepository;

@Service
public class ProjetoService {

	@Autowired
	private ProjetoRepository projetoRepository;

	public ProjetoDTO obterProjeto(Integer id) {
		return projetoRepository.findById(id)
				.orElseThrow(() -> new BusinessException("Não foi encontrado um projeto com o id informado."))
				.dto();
	}
}
