package br.com.stefanini.scrumbot.access.service;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.stefanini.scrumbot.access.exception.ReportNotFoundException;
import br.com.stefanini.scrumbot.access.exception.UnauthorizedException;
import br.com.stefanini.scrumbot.access.model.Perfil;
import br.com.stefanini.scrumbot.access.model.Relatorio;
import br.com.stefanini.scrumbot.access.repository.PerfilRepository;
import br.com.stefanini.scrumbot.access.repository.RelatorioRepository;

@Service
public class RelatorioService {

	@Autowired
	private RelatorioRepository relatorioRepository;
	
	@Autowired
	private PerfilRepository perfilRepository;

	public String obterRelatorio(Integer idProjeto, String usuario, String comando) {
		Relatorio relatorio = relatorioRepository.obterRelatorio(idProjeto, comando);
		if(Objects.isNull(relatorio)) {
			throw new ReportNotFoundException("O relatório informado não existe.");
		}
		
		Perfil perfil = perfilRepository.obterPerfil(idProjeto, relatorio.getId(), usuario);
		if(Objects.isNull(perfil)) {
			throw new UnauthorizedException("Você não tem permissão para acessar o relatório informado.");
		}

		return relatorio.getConsulta();
	}
	
	public List<String> obterComandosRelatorios(Integer idProjeto, String usuario) {
		List<Relatorio> relatorios = relatorioRepository.obterComandosRelatorios(idProjeto, usuario);
		if(Objects.isNull(relatorios)) {
			return Collections.emptyList();
		}
		return relatorios.stream().map(Relatorio::getComando).collect(Collectors.toList());
	}
}
