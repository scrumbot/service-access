package br.com.stefanini.scrumbot.access.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.stefanini.scrumbot.access.model.Relatorio;

@Repository
public interface RelatorioRepository extends CrudRepository<Relatorio, Integer>  {

	@Query("select relatorio from Relatorio relatorio "
			+ " join relatorio.projeto"
			+ " where relatorio.projeto.id = :idProjeto "
			+ " and relatorio.comando = :comando")
	public Relatorio obterRelatorio(@Param("idProjeto") Integer idProjeto, @Param("comando") String comando);
	
	@Query("select relatorio from Relatorio relatorio "
			+ " join relatorio.projeto projeto"
			+ " join relatorio.perfis perfis "
			+ " join perfis.usuarios usuarios"			
			+ " where relatorio.projeto.id = :idProjeto "
			+ " and usuarios.usuarioTelegran = :usuario")
	public List<Relatorio> obterComandosRelatorios(@Param("idProjeto") Integer idProjeto, @Param("usuario") String usuario);
}
