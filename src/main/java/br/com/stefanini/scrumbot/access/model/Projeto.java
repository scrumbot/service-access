package br.com.stefanini.scrumbot.access.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import br.com.stefanini.scrumbot.access.dto.ProjetoDTO;

@Entity
@Table(name = "projeto")
public class Projeto implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "nome")
	private String nome;

	@Column(name = "descricao")
	private String descricao;

	@Column(name = "colecao")
	private String colecao;

	@Column(name = "token_acesso")
	private String tokenAcesso;

	@Column(name = "token_bot")
	private String tokenBot;

	@Column(name = "data_criacao")
	private LocalDateTime dataCricacao;

	@Column(name = "ativo")
	private boolean ativo;

	@Column(name = "link")
	private String link;

	@OneToMany(mappedBy = "projeto")
	private List<Relatorio> relatorios;

	@OneToMany(mappedBy = "projeto")
	private List<Perfil> perfis;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getColecao() {
		return colecao;
	}

	public void setColecao(String colecao) {
		this.colecao = colecao;
	}

	public String getTokenAcesso() {
		return tokenAcesso;
	}

	public void setTokenAcesso(String tokenAcesso) {
		this.tokenAcesso = tokenAcesso;
	}

	public String getTokenBot() {
		return tokenBot;
	}

	public void setTokenBot(String tokenBot) {
		this.tokenBot = tokenBot;
	}

	public LocalDateTime getDataCricacao() {
		return dataCricacao;
	}

	public void setDataCricacao(LocalDateTime dataCricacao) {
		this.dataCricacao = dataCricacao;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public List<Relatorio> getRelatorios() {
		return relatorios;
	}

	public void setRelatorios(List<Relatorio> relatorios) {
		this.relatorios = relatorios;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public List<Perfil> getPerfis() {
		return perfis;
	}

	public void setPerfis(List<Perfil> perfis) {
		this.perfis = perfis;
	}

	public ProjetoDTO dto() {
		return new ProjetoDTO(this.id, this.nome, this.descricao, 
				this.colecao, this.tokenAcesso, this.tokenBot,
				this.dataCricacao, this.ativo, this.link);
	}

}
