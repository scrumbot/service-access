package br.com.stefanini.scrumbot.access.exception;

public class BusinessException extends RuntimeException{
	
	private static final long serialVersionUID = -4689096001172304206L;

	public BusinessException() {
		super();
	}
	
	public BusinessException(String msg) {
		super(msg);
	}
}
