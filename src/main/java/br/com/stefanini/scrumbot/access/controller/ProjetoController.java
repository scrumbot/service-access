package br.com.stefanini.scrumbot.access.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.stefanini.scrumbot.access.exception.BusinessException;
import br.com.stefanini.scrumbot.access.service.ProjetoService;

@RestController
@RequestMapping("projetos")
public class ProjetoController {

	@Autowired
	private ProjetoService projetoService;
	
	@GetMapping("/{id}")
	public ResponseEntity<Object> obterProjeto(@PathVariable("id") Integer id) {
		try {
			return ResponseEntity.ok(projetoService.obterProjeto(id));
		} catch (BusinessException e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
		}
	}
}
