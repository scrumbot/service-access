package br.com.stefanini.scrumbot.access.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.stefanini.scrumbot.access.model.Projeto;

@Repository
public interface ProjetoRepository extends CrudRepository<Projeto, Integer> {

}
