package br.com.stefanini.scrumbot.access.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.stefanini.scrumbot.access.exception.BusinessException;
import br.com.stefanini.scrumbot.access.exception.ReportNotFoundException;
import br.com.stefanini.scrumbot.access.exception.UnauthorizedException;
import br.com.stefanini.scrumbot.access.service.RelatorioService;

@RestController
@RequestMapping(value = "/relatorios")
public class RelatorioController {

	@Autowired
	private RelatorioService relatorioService;

	@GetMapping
	public ResponseEntity<String> obterRelatorio(@RequestParam("idProjeto") Integer idProjeto, 
							   @RequestParam("usuario") String usuario,
							   @RequestParam("comando") String comando) {
		try {
			return ResponseEntity.ok(this.relatorioService.obterRelatorio(idProjeto, usuario, comando));
		} catch (BusinessException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		} catch (ReportNotFoundException e) {
			return ResponseEntity.notFound().build();
		}catch (UnauthorizedException e) {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
	}
	
	@GetMapping("/{idProjeto}/{usuario}")
	public ResponseEntity<Object> obterRelatorios(@PathVariable("idProjeto") Integer idProjeto, 
							   @PathVariable("usuario") String usuario) {
		try {
			return ResponseEntity.ok(this.relatorioService.obterComandosRelatorios(idProjeto, usuario));
		} catch (BusinessException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		} 
	}

}
