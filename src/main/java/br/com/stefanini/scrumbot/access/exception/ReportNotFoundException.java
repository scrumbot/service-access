package br.com.stefanini.scrumbot.access.exception;

public class ReportNotFoundException extends RuntimeException{

	private static final long serialVersionUID = 1L;
	
	public ReportNotFoundException(){
		super();
	}
	
	public ReportNotFoundException(String msg){
		super(msg);
	}

}
