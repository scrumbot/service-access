INSERT INTO usuario
(nome, usuario_telegran)
VALUES('Tiago Sales', 'tihhjf');

INSERT INTO perfil
(nome, id_projeto)
VALUES('Perfil 1', (select id from projeto where nome = 'STEF-JF-ScrumBot'));

INSERT INTO perfil_usuario(id_perfil, id_usuario) values(
(SELECT p.id FROM perfil p where p.nome = 'Perfil 1'),
(SELECT u.id FROM usuario u where u.usuario_telegran = 'tihhjf'));

INSERT INTO relatorio(id_projeto, nome, comando, consulta, tipo_consulta) values(
(select id from projeto where nome = 'STEF-JF-ScrumBot'), 
'Tasks Doing',
'/tasks-doing',
'SELECT * FROM WorkItems WHERE [System.TeamProject] = ''@NomeProjeto'' 
AND [System.WorkItemType] = ''Task'' AND [System.State] != ''Removed'' 
AND [System.State] != ''New'' ORDER BY [System.CreatedDate]',
'DEFAULT');

INSERT INTO perfil_relatorio(id_perfil, id_relatorio) values (
(SELECT p.id FROM perfil p where p.nome = 'Perfil 1'),
(SELECT r.id FROM relatorio r where r.nome = 'Tasks Doing'));