CREATE TABLE usuario (
	id INT NOT NULL IDENTITY(1,1),
	nome varchar(300) NOT NULL,
	usuario_telegran varchar(100) NOT NULL,
	CONSTRAINT Usuario_PK PRIMARY KEY (id)
);

CREATE TABLE perfil (
	id INT NOT NULL IDENTITY(1,1),
	nome varchar(100) NOT NULL,
	id_projeto int NOT NULL,
	CONSTRAINT Perfil_PK PRIMARY KEY (id),
	CONSTRAINT Perfil_FK FOREIGN KEY (id_projeto) REFERENCES projeto(id)
);

CREATE TABLE perfil_relatorio (
	id_perfil INT NOT NULL,
	id_relatorio INT NOT NULL,
	CONSTRAINT perfil_relatorio_PK PRIMARY KEY (id_perfil,id_relatorio),
	CONSTRAINT Perfil_relatorio_perfil_FK FOREIGN KEY (id_perfil) REFERENCES perfil(id),
	CONSTRAINT Perfil_relatorio_relatorio_FK FOREIGN KEY (id_relatorio) REFERENCES relatorio(id)
);

CREATE TABLE perfil_usuario (
	id_perfil INT NOT NULL,
	id_usuario INT NOT NULL,
	CONSTRAINT perfil_usuario_PK PRIMARY KEY (id_perfil,id_usuario),
	CONSTRAINT Perfil_usuario_perfil_FK FOREIGN KEY (id_perfil) REFERENCES perfil(id),
	CONSTRAINT Perfil_usuario_usuario_FK FOREIGN KEY (id_usuario) REFERENCES usuario(id)
);