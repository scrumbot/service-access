CREATE TABLE projeto (
  id int NOT NULL IDENTITY(1,1),
  nome varchar(100) NOT NULL,
  descricao varchar(500) DEFAULT NULL,
  colecao varchar(500) DEFAULT NULL,
  token_acesso varchar(500) NOT NULL,
  token_bot varchar(500) NOT NULL,
  data_criacao datetime NOT NULL,
  ativo bit NOT NULL DEFAULT '1',
  PRIMARY KEY (id)
);

CREATE TABLE relatorio (
  id int NOT NULL IDENTITY(1,1),
  id_projeto int NOT NULL,
  nome varchar(100) NOT NULL,
  comando varchar(100) NOT NULL,
  consulta varchar(3000) NOT NULL,
  tipo_consulta varchar(100) NOT NULL DEFAULT 'DEFAULT',
  PRIMARY KEY (id),
  CONSTRAINT RELATORIO_FK FOREIGN KEY (id_projeto) REFERENCES projeto (id)
);